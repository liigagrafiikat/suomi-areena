var version = '2018-03-01';

var GS2;

init();


//#region INIT
function init(){
  CI.CellByName('ciVersion').Caption='version ' + version;
  GS2=GC.AddGS2Connection('GS2',getSetting('Server'), getSetting('Project'));
}

//#endregion



//#region CI


//#endregion



//#region SHOW

function showPage(){
  var data = {};
    data.page=AFT.RowData(1);
  gfxShowPage(data);
}

function showCredits(){
  var GS2 = GC.GetConnection('GS2');
  var template = 'credits';
  var a = GC.CreateCommandList();
  var data = getMatrixAsArray(AFT.RowData(1));
  var speed = AFT.RowData(2);
  
  if(!parseInt(speed)) speed=5;
  if(data!=-1){
  
  for (var i=0; i<data.length; i++){
    a.CopyNode('template/text','rows/eff/text'+i);
    a.Set('rows/eff/text'+i, 'Visible', true);
    a.Set('rows/eff/text'+i+'/textH', 'Text', data[i][0]);
    a.Set('rows/eff/text'+i+'/textN', 'Text', data[i][1]);
    
    if(data[i][0] == '' && data[i][1] == ''){
      a.Set('rows/eff/text'+i+'/textH', 'Opacity', [0,0,0,0]);
      a.Set('rows/eff/text'+i+'/textH', 'Text', '_');
    }
  
  }                                 
  } 
  
  a.Set('crawl', 'Speed', speed); 
  
  if(!GS2.TitleExists(template))a.DoAction('Show');
    GS2.InitTitle(template, a);
  
}

function showName(){
  var data = {};
    data.info1=AFT.RowData(1);
  gfxName(data);
}

function showNameUpper(){
  var data = {};
    data.info1=AFT.RowData(1);
  gfxNameUpper(data);
}

function showNameRed(){
  var data = {};
    data.info1=AFT.RowData(1);
  gfxNameRed(data);
}

function showName2titles(){
  var data = {};
    data.info1=AFT.RowData(1);
    data.info2=AFT.RowData(2);
    data.info3=AFT.RowData(3);
  gfxName2titles(data);
}

function showTitleSmall(){
  var data = {};
    data.info1=AFT.RowData(1);
    data.info2=AFT.RowData(2);
  gfxTitleSmall(data);
}

function showTitleLogo(){
  var data = {};
    data.info1=AFT.RowData(1);
    data.info2=AFT.RowData(2);
  gfxTitleLogo(data);
}

//#endregion



//#region GFX

function gfxClear(){
  GS2.RemoveAllTitles();
}

function gfxClearExcept(exceptions){
  GS2.RemoveAllTitlesExcept(exceptions);
}

function gfxShowPage(data){
  var template = data.page;
  var a=GC.CreateCommandList();
  
  a.DoAction('Show');
  GS2.InitTitle(template,a);
}

function gfxCredits(data){
  var template = 'credits';
  var a=GC.CreateCommandList();
  a.Set('name', 'Text', data.text);
  a.Set('title', 'Text', data.text);
  if(parseFloat(data.speed,10)>0) a.Set('crawl', 'Speed', parseFloat(data.speed,10));
  a.DoAction('Show');
  GS2.InitTitle(template,a);
}

function gfxName(data){
  var template = 'name-1R';
  var a=GC.CreateCommandList();
    
  a.Set('name', 'Text', data.info1);
  
  a.DoAction('Show');
  GS2.InitTitle(template,a);
}

function gfxNameUpper(data){
  var template = 'name-upper';
  var a=GC.CreateCommandList();
    
  a.Set('name', 'Text', data.info1);
  
  a.DoAction('Show');
  GS2.InitTitle(template,a);
}

function gfxNameRed(data){
  var template = 'host';
  var a=GC.CreateCommandList();
    
  a.Set('name', 'Text', data.info1);
  
  a.DoAction('Show');
  GS2.InitTitle(template,a);
}

function gfxName2titles(data){
  var template = 'name-title-2R';
  var a=GC.CreateCommandList();
    
  a.Set('name', 'Text', data.info1);
  a.Set('titleUpper', 'Text', data.info2);
  a.Set('titleLower', 'Text', data.info3);
  
  a.DoAction('Show');
  GS2.InitTitle(template,a);
}

function gfxTitleSmall(data){
  var template = 'title';
  var a=GC.CreateCommandList();
    
  a.Set('name', 'Text', data.info1);
  a.Set('titleUpper', 'Text', data.info2);
  
  a.DoAction('Show');
  GS2.InitTitle(template,a);
}

function gfxTitleLogo(data){
  var template = 'title-logo';
  var a=GC.CreateCommandList();
    
  a.Set('name', 'Text', data.info1);
  a.Set('titleUpper', 'Text', data.info2);
  
  a.DoAction('Show');
  GS2.InitTitle(template,a);
}

//#endregion



//#region HELPERS

function getCurrentUnixTime() {
  var d=new Date();
  var time=d.getTime();
  return time;
}


function getMatrixAsArray(sMatrixName) {
  if (!Matrices.Exists(sMatrixName)) {
    alert('Could not find the matrix with the name ' + sMatrixName);
    return;
  } 
  var matrix = Matrices.GetMatrix(sMatrixName);    
  return matrix.toRowArray();
}


function getSetting(Setting){
  var ret ='';
  var mtx = Matrices.GetMatrix('Settings');
  var idx=mtx.FindRow(Setting,1);
  if(idx>0) ret=mtx.Cell(2,idx).Value; 
  return ret;
}


function log(st) {
  var d= new Date();
  CILog.Insert(0,d.toLocaleTimeString()+ ' ' + st);
  while (CILog.Count>100) CILog.Delete(CILog.Count-1);
}


//#endregion



